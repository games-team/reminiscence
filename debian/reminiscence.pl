#!/usr/bin/perl

# Copyright 2010 Damyan Ivanov <dmn@debian.org>
# Permission is granted to use this work, with or without modifications,
# provided that this notice is retained. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return.

# documentation is at the end

use strict;
use warnings;

my $data = "$ENV{HOME}/.reminiscence/data";
my $save = "$ENV{HOME}/.reminiscence/save";
my $start_level = 0;
my $rs = '/usr/games/rs';

use Pod::Usage;

use Getopt::Long;

my $r = GetOptions(
    'datapath=s'    => \$data,
    'savepath=s'    => \$save,
    'levelnum=s'    => \$start_level,
    'help' => sub { pod2usage( -exitcode => 0, -verbose => 1 ) },
) or pod2usage(2);

-e $data or die "Data directory $data does not exist.\n";
-d $data or die "$data is not a directory.\n";

use File::Path qw(mkpath);

if ( -e $save ) {
    -d $save or die "$save is not a directory.\n";
}
else {
    mkpath($save) or die "mkdir($save): $!\n";
}

exec "$rs --datapath=$data --savepath=$save --levelnum=$start_level";

__END__

=head1 NAME

reminiscence - a wrapper around rs with changed defaults

=head1 SYNOPSIS

    reminiscence
    reminiscence --datapath path
    reminiscence --savepath path
    reminiscence --levelnum num
    reminiscence --help

=head1 DESCRITION

B<reminiscence> is a tiny wrapper for /usr/games/rs, the REminiscence engine.
It supports the same options as L<rs(6)>. The wrapper bails out if the data
directory does not exist. It will also create the save game directory if
needed.

=head1 OPTIONS

=over

=item B<--datapath> I<path>

Use I<path> for locating game data. Defaults to I<.reminiscence/data> in
user's home directory. B<reminiscence> exits with an error if the directory
does not exist.

=item B<--savepath> I<path>

Use I<path> for game saves. Defaults to I<.reminiscence/save> in user's home
directory. The directory is created if needed.

=item B<--levelnum> I<num>

Start from level I<num>. Default is 0.

=item B<--help>

Display short usage info.

=back

=head1 SEE ALSO

=over

=item L<rs(6)>

=back

=head1 COPYRIGHT AND LICENSE

Copyright 2010, 2011 Damyan Ivanov L<dmn@debian.org>

Permission is granted to use this work, with or without modifications, provided
that this notice is retained. If we meet some day, and you think this stuff is
worth it, you can buy me a beer in return.

=cut
